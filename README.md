### 介绍

将静态图转化为分块加载的动态图

### 方案
```
1. PIL: 
    1. 创建背景图
    2. 将原图拆分成N块并依次合成到背景图的相应位置, 得到N张素材图
    3. 将N张素材图合成GIF

2. pygifsicle
    对合成的GIF进行优化(无损压缩, 精简体积)
    注意: 需要电脑安装gifsicle, 官网: https://www.lcdf.org/gifsicle/, 
    若看不懂英文, 网上资料一大把, (其实不安装也不影响正常使用, 只是没有优化GIF而已)

3. tkinter:
    用于图形化界面的实现, 便于操作
    
4. pyinstaller
    用于将脚本打包成exe
```

### img2gif.py
```bash
简介: 将图片转成gif 命令行模式
使用: python img2gif.py -h  
示例: python img2gif.py -p /Users/tianshl/Documents/sample.jpg
```
### img2gif_gui.py
```bash
简介: 将图片转成gif 图像化界面
使用: python img2gif_gui.py
```

### 打包成exe
```
pyinstaller -F -w -i gif.ico img2gif_gui.py
# 执行完指令后, exe文件在dist目录下
# 我打包的exe: https://download.csdn.net/download/xiaobuding007/12685554
```

